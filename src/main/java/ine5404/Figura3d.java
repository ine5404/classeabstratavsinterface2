/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ine5404;

/**
 *
 * @author martin.vigil
 */
public abstract class Figura3d extends Figura2d {
    protected int z;

    public Figura3d(int x, int y, int z){
        super(x, y);
        this.z = z;
    }
    
    public void mover(int x, int y, int z){
        super.mover(x, y);
        this.z = z;
    }
    
    public abstract double getVolume();
    
}
