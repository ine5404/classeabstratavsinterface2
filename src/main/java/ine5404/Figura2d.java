/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ine5404;

/**
 *
 * @author martin.vigil
 */
public abstract class Figura2d {
    protected int x,y;
    
    public Figura2d(int x, int y){
        this.x = x;
        this.y = y;        
    }
    
    public void mover(int x, int y){
        this.x = x;
        this.y = y;
    }
    
    public abstract double getArea();
}
