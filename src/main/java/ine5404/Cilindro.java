/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ine5404;

/**
 *
 * @author martin.vigil
 */
public class Cilindro extends Figura3d {
    protected double altura;
    protected Circulo circulo;
    
    public Cilindro(int x, int y, int z, double raio, double altura){
        super(x, y, z);
        this.circulo = new Circulo(x, y, raio);
        this.altura = altura;
    }
    
    @Override
    public double getVolume() {
        return this.circulo.getArea() * this.altura;
    }
    
    @Override 
    public double getArea(){
        return 2 * this.circulo.getArea() + this.circulo.getPerimetro() * this.altura;
    }
}
