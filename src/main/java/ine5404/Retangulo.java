/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ine5404;

/**
 *
 * @author martin.vigil
 */
public class Retangulo extends Figura2d {
    protected double altura, base;

    public double getAltura() {
        return altura;
    }

    public double getBase() {
        return base;
    }
    
    public Retangulo(int x, int y, double base, double altura){
        super(x,y);
        this.altura = altura;
        this.base = base;
    }
    
    @Override
    public double getArea() {
        return this.base * this.altura;
    }
    
}
