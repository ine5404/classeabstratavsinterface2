/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ine5404;

/**
 *
 * @author martin.vigil
 */
public class Bloco extends Figura3d {
    protected double base, altura, profundidade;
    
    public Bloco(int x, int y, int z, double base, double altura, double profundidade){
        super(x, y, z);
        this.base = base;
        this.altura = altura;
        this.profundidade = profundidade;                
    }
    
    @Override
    public double getArea() {
        return 2 * (this.base * this.altura + this.base * this.profundidade + this.altura * this.profundidade);
    }
    
    @Override
    public double getVolume() {
        return this.base * this.altura * this.profundidade;
    }

}
